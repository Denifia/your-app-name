﻿using System;
using System.Collections.Generic;
using FacebookApp.Domain;
using System.Data.Entity;

namespace FacebookApp.Data
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Gift> Gifts { get; set; }
    }
}
