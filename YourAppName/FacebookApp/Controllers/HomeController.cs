﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacebookApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var context = new FacebookApp.Data.DatabaseContext();


            ViewBag.Message = string.Format("There are {0} user{1}!", context.Users.Count(), context.Users.Count() == 1 ? "" : "s");

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
