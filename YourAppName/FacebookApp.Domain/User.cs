﻿using System;
using System.Collections.Generic;

namespace FacebookApp.Domain
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FaceBookUid { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual IList<Gift> Gifts { get; set; }
    }
}
