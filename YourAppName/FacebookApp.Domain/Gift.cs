﻿namespace FacebookApp.Domain
{
    public class Gift
    {
        public int GiftId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
    }
}
